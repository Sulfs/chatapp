import chatService from '../services/chat.service';

export default {
    join,
    sendMessage
}

function join(username, room) {
    return dispatch => {
        dispatch(request(username, room));
        chatService.join(username, room, messages => dispatch({ type: 'SYNC_SUCCESS', messages }), error => dispatch({ type: 'FAILURE', error })
        ).then(
            messages => dispatch(success(messages)),
            error => dispatch(failure(error))
        );
    };

    function request(username, room) { return { type: 'JOIN_REQUEST', username, room } }
    function success(messages) { return { type: 'JOIN_SUCCESS', messages } }
    function failure(error) { return { type: 'FAILURE', error } }
}

function sendMessage(message) {
    return dispatch => {
        dispatch({ type: 'SEND_MESSAGE_REQUEST', message });
        chatService.sendMessage(message).then(message => dispatch({ type: 'SEND_MESSAGE_SUCCESS', message })).catch(error => dispatch({ type: 'FAILURE', error }))
    }
}