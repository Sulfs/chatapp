import React from 'react';
import chatActions from '../actions/chat.actions';
import { connect } from 'react-redux';
import { View, Text, Button, TextInput, StyleSheet, SafeAreaView } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import moment from "moment";

@connect(({ chat }) => ({ chat }))

export default class Chat extends React.PureComponent {

  state =
  {
    newMessage: "",
    messages : []
  }

  handleSend = () =>
  {
    const { route: { params }, dispatch} = this.props;
    const { username } = params;
    const { newMessage, messages } = this.state;


    dispatch(chatActions.sendMessage({
        author: username,
        content: newMessage,
    }));

    this.setState({
     newMessage: ''
    });
   }

  renderItem = ({item: message}) =>
  {
    const { route: { params }} = this.props;
    const { username } = params;

    return(
      <View key={message.id} style={[
        styles.message,
        username == message.author && styles.authorMessage
      ]}>
        <View style={[
          styles.bubble,
          username == message.author && styles.authorBubble
        ]}>
          <Text style={[
            styles.messageAuthor,
            username == message.author && { color: '#ddd' }
          ]}>
            {message.author}
          </Text>
          <Text style={[
            styles.messageContent,
            username == message.author && { color: '#ddd' }
          ]}>
            {message.content}
          </Text>
        </View>
        <Text style={styles.timestamp}>
          {moment(message.created_at).format('DD/MM hh:mm')}
        </Text>
      </View>
    )
  }

  render() {
    const { newMessage } = this.state;
    const { chat, route: { params }} = this.props;
    const { username, room } = params;
    const { messages, loading } = chat;

    const Empty = () => (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text> No messages yet</Text>
        </View>
    );

    const Loading = () => (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <Text>loading...</Text>
                </View>
    );

    return (
    <SafeAreaView style = {{ flex: 1 }}>

        {loading
        ? <Loading />
        : !messages.length
        ? <Empty />
        : <FlatList
                 data={messages}
                  renderItem={this.renderItem}
                  keyExtractor={item => item._id}
                  ref={ref => this.listRef = ref}
                  onContentSizeChange={() => this.listRef.scrollToEnd({animated: true})}
                  onLayout={() => this.listRef.scrollToEnd({animated: true})}
        />
        }

        <View style={styles.messageComposer}>
          <TextInput style={styles.textInput}
            onChangeText={newMessage => this.setState({newMessage})}
            value={newMessage}
          />
          <Button title="SEND"
            onPress={this.handleSend}
            disabled={newMessage == ""}
          />
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
    messageList: {
      flex: 1,
    },
    message: {
      padding: 8,
      justifyContent: 'flex-start',
      alignItems: 'flex-start'
    },
    authorMessage: {
      alignItems: 'flex-end',
    },
    bubble: {
      padding: 8,
      backgroundColor: '#CCC',
      maxWidth: '80%',
      borderRadius: 16,
      borderBottomLeftRadius: 0
    },
    authorBubble: {
      backgroundColor: '#0000AA',
      borderBottomLeftRadius: 16,
      borderBottomRightRadius: 0
    },
    timestamp: {
      color: '#999999',
      fontSize: 12
    },
    messageAuthor: {
      fontWeight: 'bold',
      color: '#222'
    },
    messageContent: {
      color: '#333'
    },
    messageComposer: {
      flex: 0,
      flexWrap: 'nowrap',
      flexDirection: 'row',
      padding: 4,
      backgroundColor: '#DDDDDD',
      borderTopWidth: 1,
      borderColor: '#AAA',
    },
    textInput: {
      borderWidth: 1,
      borderColor: '#AAA',
      padding: 4,
      flex: 1,
      backgroundColor: '#FFFFFF'
    },
});
