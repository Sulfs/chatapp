const initState = {
    messages: [],
    username: null,
    room: null,
    error: null,
    loading: false
}

export default function(state=initState, action) {
    console.log(action);
    switch(action.type) {
    case 'JOIN_REQUEST':
        return {
            ...state,
            room: action.room,
            username: action.username,
            error: null,
            loading: true,
            messages: []
        };
    case 'JOIN_SUCCESS':
    case 'SYNC_SUCCESS':
        return {
            ...state,
            messages: action.messages,
            loading: false
        };

    case 'SEND_MESSAGE_SUCCESS':
        return{
            ...state,
            messages: [
            ...state.messages,
            action.message
           ]
        }

    case 'FAILURE':
        return{
            ...state,
            error: action.error,
            loading:false
        }
    default:
        return state;
    }
}