import PouchDB from 'pouchdb-react-native'

const REMOTE_DB = "http://admin:veryverysecret@gathor.org:8923";

class Chat {

    room = '';
    username = '';
    db = null;
    sync = null;

    join(username,room, onSync, onFailure){
        this.username = username || 'Anonymous';
        this.room = room.toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "") || 'default';
        this.name = `chat-room-$(this.room)`;

        this.db = new PouchDB(this.room);
        this.sync = this.db.sync(`${REMOTE_DB}/${this.room}`, {
            live : true,
            retry : true,
            continuous : true
        });
        this.sync.on('change', this.handleChange(onSync));
        this.sync.on('error', this.handleError(onFailure));

        return this.getMessages();

    }

    handleChange = onSync => {
        return async e => {
            const messages = await this.getMessages();
            onSync(messages);
        }
    }

    handleError = onFailure => {
        return error => {
            onFailure(error);
        }
    }

    async getMessages() {
        if(!this.db) throw 'No database';
        try {
            const response = await this.db.allDocs ({ include_docs: true});
            return response.rows.map(r => r.doc).sort((a,b) => a.created_at > b.created_at);
        } catch(e){
            console.error(e);
        }
    }

    async sendMessage({ author, content }) {
        if(!this.db) throw 'No dtaabse';
        try{
            const message = {
                author,
                content,
                created_at: new Date()
            };
            const response = await this.db.post(message);
            return {
                _id: response.id,
                ...message
            };
        } catch(e){
            console.error(e);
        }
    }
}

export default new Chat();